FROM node

RUN mkdir /home/app
WORKDIR /home/app
COPY . /home/app

COPY package.json /app
RUN yarn install

EXPOSE 3000

#RUN yarn test
RUN yarn build

CMD yarn start